#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QList>
#include "Calculator.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
//Public fields

//

//Private fields
private:
    Ui::MainWindow *ui;
    Calculator* calculator;
//

//Public methods
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
//


//Private methods
private:
    void InitializationUI();
    void Initialization();
    void EnterNumber(char);
    void EnterOperator(char);
    void UpdateView();
    void UpdateResult();
    void Clear();
//

//Slots
private slots:
    void Button0();
    void Button1();
    void Button2();
    void Button3();
    void Button4();
    void Button5();
    void Button6();
    void Button7();
    void Button8();
    void Button9();

    void OperatorPlus();
    void OperatorMinus();
    void OperatorMultiply();
    void OperatorDivision();

    void ButtonClear();
    void ButtonResult();
//
};
#endif // MAINWINDOW_H
