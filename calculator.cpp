#include "Calculator.h"

//Public methods
Calculator::Calculator()
{
    numbers = new QList<int>();
    operators = new QList<char>();
    currentNumber = new QString();
}

Calculator::~Calculator()
{
    delete numbers;
    delete currentNumber;
}

void Calculator::AddNumber(char c)
{
    currentNumber->append(c);
}

void Calculator::AddOperator(char c)
{
    if (currentNumber->count() == 0) return;
    operators->append(c);
    numbers->append(currentNumber->toInt());
    currentNumber->clear();
}

void Calculator::Clear()
{
    operators->clear();
    numbers->clear();
    currentNumber->clear();
}

double Calculator::GetResult()
{
    return Evaluate();
}

QString* Calculator::ToString()
{
    QString* string = new QString();
    if (numbers->count() != 0)
    {
        string->append(QString::number(numbers->at(0)));
        int j = 0;
        for (int i = 1;i<numbers->count();i++)
        {
            string->append(operators->at(j));
            string->append(QString::number(numbers->at(i)));
            j++;
        }
        if (j<operators->count())
            string->append(operators->at(j));
    }
    string->append(*currentNumber);
    return string;
}
//

//Private methods
double Calculator::Evaluate()
{
    if (numbers->count() == 0 || operators->count() == 0) return currentNumber->toDouble();
    int j = 0;
    double result = numbers->at(0);
    for (int i = 1;i<numbers->count();i++)
    {
        result = ParseOperator(operators->at(j), result, numbers->at(i));
        j++;
    }

    if (j<operators->count() && currentNumber->count() > 0)
        result = ParseOperator(operators->at(j), result, currentNumber->toDouble());

    return result;
}

double Calculator::ParseOperator(char _operator, double prevResult, double newValue)
{
    switch (_operator)
    {
        case '+':
            prevResult += newValue;
            break;
        case '-':
            prevResult -= newValue;
            break;
        case '*':
            prevResult *= newValue;
            break;
        case '/':
            prevResult /= newValue;
            break;
    }
    return prevResult;
}
//
