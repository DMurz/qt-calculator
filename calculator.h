#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QList>

class Calculator
{
//Public fields

//
//Private fields
private:
    QList<int>* numbers;
    QList<char>* operators;
    QString* currentNumber;
//
//Public methods
public:
    Calculator();
    ~Calculator();
    void AddNumber(char);
    void AddOperator(char);
    void Clear();
    double GetResult();
    QString* ToString();
//
//Private methods
    double Evaluate();
    double ParseOperator(char, double, double);
//
};

#endif // CALCULATOR_H
