#include "mainwindow.h"
#include "ui_mainwindow.h"

//Public methods
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    Initialization();
    InitializationUI();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete calculator;
}
//

//Private methods
void MainWindow::EnterNumber(char num)
{
    calculator->AddNumber(num);
    UpdateView();
}

void MainWindow::EnterOperator(char c)
{
    calculator->AddOperator(c);
    UpdateView();
}

void MainWindow::Initialization()
{
    calculator = new Calculator();
}

void MainWindow::UpdateView()
{
    QString* history = calculator->ToString();
    ui->LabelHistory->setText(*history);
    delete history;

    UpdateResult();
}

void MainWindow::UpdateResult()
{
    double result = calculator->GetResult();
    ui->LabelResult->setText(QString::number(result));
}

void MainWindow::Clear()
{
    calculator->Clear();
    UpdateView();
}

void MainWindow::InitializationUI()
{
    connect(ui->ButtonNumber0, &QPushButton::clicked, this, &MainWindow::Button0);
    connect(ui->ButtonNumber1, &QPushButton::clicked, this, &MainWindow::Button1);
    connect(ui->ButtonNumber2, &QPushButton::clicked, this, &MainWindow::Button2);
    connect(ui->ButtonNumber3, &QPushButton::clicked, this, &MainWindow::Button3);
    connect(ui->ButtonNumber4, &QPushButton::clicked, this, &MainWindow::Button4);
    connect(ui->ButtonNumber5, &QPushButton::clicked, this, &MainWindow::Button5);
    connect(ui->ButtonNumber6, &QPushButton::clicked, this, &MainWindow::Button6);
    connect(ui->ButtonNumber7, &QPushButton::clicked, this, &MainWindow::Button7);
    connect(ui->ButtonNumber8, &QPushButton::clicked, this, &MainWindow::Button8);
    connect(ui->ButtonNumber9, &QPushButton::clicked, this, &MainWindow::Button9);

    connect(ui->ButtonOperatorDivision, &QPushButton::clicked, this, &MainWindow::OperatorDivision);
    connect(ui->ButtonOperatorMultiply, &QPushButton::clicked, this, &MainWindow::OperatorMultiply);
    connect(ui->ButtonOperatorPlus, &QPushButton::clicked, this, &MainWindow::OperatorPlus);
    connect(ui->ButtonOperatorMinus, &QPushButton::clicked, this, &MainWindow::OperatorMinus);

    connect(ui->ButtonClear, &QPushButton::clicked, this, &MainWindow::ButtonClear);
    connect(ui->ButtonResult, &QPushButton::clicked, this, &MainWindow::ButtonResult);
}
//

//Slots
void MainWindow::Button0()
{
    EnterNumber('0');
}
void MainWindow::Button1()
{
    EnterNumber('1');
}
void MainWindow::Button2()
{
    EnterNumber('2');
}
void MainWindow::Button3()
{
    EnterNumber('3');
}
void MainWindow::Button4()
{
    EnterNumber('4');
}
void MainWindow::Button5()
{
    EnterNumber('5');
}
void MainWindow::Button6()
{
    EnterNumber('6');
}
void MainWindow::Button7()
{
    EnterNumber('7');
}
void MainWindow::Button8()
{
    EnterNumber('8');
}
void MainWindow::Button9()
{
    EnterNumber('9');
}

void MainWindow::OperatorPlus()
{
    EnterOperator('+');
}
void MainWindow::OperatorMinus()
{
    EnterOperator('-');
}
void MainWindow::OperatorMultiply()
{
    EnterOperator('*');
}
void MainWindow::OperatorDivision()
{
    EnterOperator('/');
}

void MainWindow::ButtonClear()
{
    Clear();
}
void MainWindow::ButtonResult()
{
    UpdateResult();
}
//
